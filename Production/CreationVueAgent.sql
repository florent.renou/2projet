USE [Projet]
GO

CREATE view [dbo].[PacksetRH_Agent] as 
Select P_NOM,P_PREN,G.GRADE,CORP,OTA_NUM,OTA_NUM_IDENT,OTA_NUM_SERIE,
ART_LIBELLE,ENT_LIBELLE,OTA_DATE_ACHA,OTA_DATE_SERV
from dbo.RECEPTION AS R
INNER JOIN dbo.OTHER_ART AS O ON  R.REC_MAT_CLE = O.OTA_CLE
INNER JOIN dbo.ARTICLE AS A ON  O.OTA_ART_CLE = A.ART_CLE
INNER JOIN dbo.ENTITE AS E ON A.ART_ENT_CLE = E.ENT_CLE
INNER JOIN dbo.POMPERS ON R.REC_TYPE_CLE = POMPERS.P_CLE
INNER JOIN dbo.POMPCOR ON R.REC_CIS_CLE = POMPCOR.CLECOR
INNER JOIN dbo.SP_GRADE AS G ON POMPERS.P_GRAD = G.CLE
WHERE REC_TYPE = 'AGENT';

GO

