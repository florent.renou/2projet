@echo off
REM N�cessaire pour l'utilisation des variable dans la boucle FOR
setlocal enableDelayedExpansion

::---------------------------------------------------------------------------------------------------------------------
::Le premier bloc g�re l'extraction des donn�es gr�ce � la commande SQLCMD qui permet, dans le cas du script, de 
::g�n�rer une requ�te, stocker dans le fichier "Requete_XXX.sql", et �crire le r�sultat dans un fichier "XXX.csv".
::---------------------------------------------------------------------------------------------------------------------

REM Premi�re commande permettant de r�cup�rer les donn�es des agents.
sqlcmd -U SCRIPT -P P@ssword -S DESKTOP-G6GEQF2\SQLEXPRESSLOCAL -s";" -W -i .\Requete_agent.sql -o .\agent.csv
REM Deuxi�me commande permettant de r�cup�rer les donn�es des CIS.
sqlcmd -U SCRIPT -P P@ssword -S DESKTOP-G6GEQF2\SQLEXPRESSLOCAL -s";" -W -i .\Requete_cis.sql -o .\cis.csv
REM Troisi�me commande permettant de r�cup�rer les donn�es des stocks.
sqlcmd -U SCRIPT -P P@ssword -S DESKTOP-G6GEQF2\SQLEXPRESSLOCAL -s";" -W -i .\Requete_stock.sql -o .\stock.csv

::---------------------------------------------------------------------------------------------------------------------
::Le deuxi�me bloc initialise les variables permettant d'int�grer la date du jour dans les noms de fichier qui seront
::fournis aux utilisateurs.
::---------------------------------------------------------------------------------------------------------------------

REM L'initialisation de l'ann�e dans la variable a.
set annee=%DATE:~6,4%
REM L'initialisation du moi dans la variable m.
set mois=%DATE:~3,2%
REM L'initialisation du jour dans la variable j.
set jour=%DATE:~0,2%

::---------------------------------------------------------------------------------------------------------------------
::Le troisi�me bloc sert � "peaufiner" le ficher qui a �t� g�n�rer pr�c�demment. Il supprime la deuxi�me ligne du
::fichier qui n'est pas n�cessaire et nomme le fihcier avec la date � laquelle l'extraction est faite.
::---------------------------------------------------------------------------------------------------------------------

REM Test que le fichier pour les agents n'a pas d�j� �t� g�n�r� dans la journ�e et si oui alors le supprime.
if exist ".\RES_agent_%jour%_%mois%_%annee%.csv" del .\RES_agent_%jour%_%mois%_%annee%.csv 
REM R�cup�re la premi�re ligne du fichier g�n�rer pour obtenir les noms des colonnes.
set /p ligne=<.\agent.csv
REM Cr�e le fichier � destination des utilisateurs avec la date dans le nom et la ligne r�cup�r� juste avant.
echo %ligne% > .\RES_agent_%jour%_%mois%_%annee%.csv
REM R�cup�re toutes les lignes du fichier g�n�r� sauf les deux premi�re et les r�injecte dans le fichier cr�e.
for /f "skip=2 tokens=1,2,3,4,5,6,7,8,9,10,11,12 delims=;" %%a in ('type .\agent.csv') do (
REM Ajout d'une tabulation � chaque champ pour une meilleure mise en pages.
echo %%a	;%%b	;%%c	;%%d	;%%e	;%%f	;%%g	;%%h	;%%i	;%%j	;%%k	;%%l	>> .\RES_agent_%jour%_%mois%_%annee%.csv
)

REM Test que le fichier pour les CIS n'a pas d�j� �t� g�n�r� dans la journ�e et si oui alors le supprime.
if exist ".\RES_cis_%jour%_%mois%_%annee%.csv" del .\RES_cis_%jour%_%mois%_%annee%.csv
REM R�cup�re la premi�re ligne du fichier g�n�rer pour obtenir les noms des colonnes.
set /p ligne=<.\cis.csv
REM Cr�e le fichier � destination des utilisateurs avec la date dans le nom et la premi�re ligne r�cup�r� juste avant.
echo %ligne% > .\RES_cis_%jour%_%mois%_%annee%.csv
::---------------------------------------------------------------------------------------------------------------------
::Traitement particulier pour les donn�es CIS, n�cessiter de regrouper les donn�es du champ ETAT.
::---------------------------------------------------------------------------------------------------------------------
REM Initialisation la variable permettant d'�viter l'insertion d'une mauvaise ligne lors du premier passage dans la boucle.
set condition=0
REM L'initialisation de la variable qui sert de stockage pour le champ CENTRE.
set aa=0
REM L'initialisation de la variable qui sert de stockage pour le champ NUMERO.
set bb=0
REM L'initialisation de la variable qui sert de stockage pour le champ IDENTITE.
set cc=0
REM L'initialisation de la variable qui sert de stockage pour le champ SERIE.
set dd=0
REM L'initialisation de la variable qui sert de stockage pour le champ ARTICLE.
set ee=0
REM L'initialisation de la variable qui sert de stockage pour le champ ENTITE.
set ff=0
REM L'initialisation de la variable qui sert de stockage pour le champ DATE_ACHAT.
set gg=0
REM L'initialisation de la variable qui sert de stockage pour le champ DATE_SERVICE.
set hh=0
REM L'initialisation de la variable qui sert de stockage pour le champ TEMPS_SERVICE.
set ii=0
REM L'initialisation de la variable qui sert de stockage pour le champ ETAT.
set jj=0
REM D�but de la boucle, r�cup�ration de la ligne dans le fichier et attribution des diff�rent champ dans les variables.
for /f "skip=2 tokens=1,2,3,4,5,6,7,8,9,10 delims=;" %%a in ('type .\cis.csv') do (
REM Test pour savoir si la ligne parcourut � les m�mes valeurs que la pr�c�dente hormis l'�tat.
if %%b == !bb! ( if %%c == !cc! ( if %%d == !dd! (
REM Alors on stock l'�tat qui, lui, est diff�rent.
set jj=!jj! / %%j
REM Sinon
) ) ) else (
REM Test pour �viter l'ajout de la ligne lors du premier passage.
if !condition!==1 echo !aa!	;!bb!	;!cc!	;!dd!	;!ee!	;!ff!	;!gg!	;!hh!	;!ii!	;!jj!>> .\RES_cis_%jour%_%mois%_%annee%.csv
REM Stock les nouvelles donn�es du champ CENTRE dans la variable pr�vue � cet effet.
set aa=%%a
REM Stock les nouvelles donn�es du champ NUMERO dans la variable pr�vue � cet effet.
set bb=%%b
REM Stock les nouvelles donn�es du champ IDENTITE dans la variable pr�vue � cet effet.
set cc=%%c
REM Stock les nouvelles donn�es du champ SERIE dans la variable pr�vue � cet effet.
set dd=%%d
REM Stock les nouvelles donn�es du champ ARTICLE dans la variable pr�vue � cet effet.
set ee=%%e
REM Stock les nouvelles donn�es du champ ENTITE dans la variable pr�vue � cet effet.
set ff=%%f
REM Stock les nouvelles donn�es du champ DATE_ACHAT dans la variable pr�vue � cet effet.
set gg=%%g
REM Stock les nouvelles donn�es du champ DATE_SERVICE dans la variable pr�vue � cet effet.
set hh=%%h
REM Stock les nouvelles donn�es du champ TEMPS_SERVICE dans la variable pr�vue � cet effet.
set ii=%%i
REM Stock les nouvelles donn�es du champ ETAT dans la variable pr�vue � cet effet.
set jj=%%j
REM Permet l'ajout des lignes apr�s le premier passage dans la boucle.
set /a condition=1
)
)
::---------------------------------------------------------------------------------------------------------------------

REM Test que le fichier pour les stocks n'a pas d�j� �t� g�n�r� dans la journ�e et si oui alors le supprime.
if exist ".\RES_stock_%jour%_%mois%_%annee%.csv" del .\RES_stock_%jour%_%mois%_%annee%.csv
REM R�cup�re la premi�re ligne du fichier g�n�rer pour obtenir les noms des colonnes.
set /p ligne=<.\stock.csv
REM Cr�e le fichier � destination des utilisateurs avec la date dans le nom et la ligne r�cup�r� juste avant.
echo %ligne% > .\RES_stock_%jour%_%mois%_%annee%.csv
REM R�cup�re toutes les lignes du fichier g�n�r� sauf les deux premi�re et les r�injecte dans le fichier cr�e.
for /f "skip=2 tokens=1,2,3,4,5,6,7 delims=;" %%a in ('type .\stock.csv') do (
REM Ajout d'une tabulation � chaque champ pour une meilleure mise en pages.
echo %%a	;%%b	;%%c	;%%d	;%%e	;%%f	;%%g	>> .\RES_stock_%jour%_%mois%_%annee%.csv
)